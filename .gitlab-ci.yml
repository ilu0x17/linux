---
include:
  - project: "revolutionpi/infrastructure/ci-templates"
    file: "base.yml"
  - project: "revolutionpi/infrastructure/ci-templates"
    file: "kernel-devel-package.yml"

checkpatch:
  stage: test
  image: debian:bookworm-slim
  tags:
    - self-hosted
    - host-arm64
  before_script:
    # install dependencies
    - apt-get update
    - apt-get install --no-install-recommends -y curl jq ca-certificates codespell libcolor-ansi-util-perl git
  script:
    # fetch commit list from gitlab API
    - COMMITS=$(curl -sL "http://gitlab.com/api/v4/projects/$CI_PROJECT_ID/merge_requests/${CI_MERGE_REQUEST_IID}/commits"  | jq -r '.[].id')
    # run checkpatch on each commit
    - RC=0
    - >
      for commit in $COMMITS; do
        echo -e "\e[0Ksection_start:`date +%s`:${commit}\r\e[0KCOMMIT ${commit}"
        ./scripts/checkpatch.pl --ignore FILE_PATH_CHANGES --color=never --codespell --git "${commit}" || RC=1
        echo -e "\e[0Ksection_end:`date +%s`:${commit}\r\e[0K"
      done
    # let job fail, if at least one commit has errors
    - exit $RC
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
  allow_failure: true

trigger-lava-ci:
  dependencies:
    - build-kernel
  variables:
    IMAGE_URL: "https://artifactory.kunbus.com:443/artifactory/revpi-ci-artifacts/debos-build/240701_1355813865/6a02c16f5f2a_2024-07-01-revpi-bookworm-arm64-default.img.xz"
    DEBIAN_FRONTEND: noninteractive
  # The test stage cannot be assigned because this job depends on the build kernel, which is in an earlier stage. The stages are defined in base.yml
  stage: publish
  image: "registry.gitlab.com/revolutionpi/infrastructure/docker-images/build-container/build-container-bookworm:arm64-latest"
  tags:
    - self-hosted
    - host-arm64
  before_script:
    - sudo apt update && sudo apt install -y jq
    - |
      export pkg_name="$(printf "%s_snapshot-%s" \
        "$(date +%F)" \
        "$CI_PIPELINE_IID")"
    - export ARTIFACTORY_API_URL="$ARTIFACTORY_URL/api/storage"
    - export ARTIFACTS_URL="$ARTIFACTORY_URL/$ARTIFACTORY_UPLOAD_DIR/"
  script:
    - |
      artifacts=$(curl \
        -s \
        -u "$ARTIFACTORY_USER:$ARTIFACTORY_TOKEN" \
        "$ARTIFACTORY_API_URL/$ARTIFACTORY_UPLOAD_DIR/" | jq -r '.children | .[].uri | select(. | test("^(?!.*(libc|dbg)).*deb$"))')

    - >
      artifacts_urls=""

      for artifact in $artifacts; do
        artifacts_urls="${artifacts_urls} $ARTIFACTS_URL$artifact"
      done
    - echo "artifacts_urls $artifacts_urls"
    # project id 51317347:
    # https://gitlab.com/revolutionpi/infrastructure/testing/lava-ci
    #
    # NOTE: the manually-created trigger token for lava-ci needs to be used for
    # it to be able to tell if the pipeline was started from a trigger or not.
    - |
      curl \
        -f \
        --request POST \
        --form "token=$LAVA_CI_GITLAB_TRIGGER_TOKEN" \
        --form "ref=master" \
        --form "variables[PKG_URL]=$artifacts_urls" \
        --form "variables[IMAGE_URL]=$IMAGE_URL" \
        "https://gitlab.com/api/v4/projects/51317347/trigger/pipeline"
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule"
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      when: manual
      allow_failure: true
    - when: manual
